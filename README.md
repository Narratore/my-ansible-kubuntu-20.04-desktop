# Ansible provisions Kubuntu for dev and admin

Please install ansible, now is a official distribution package

```
$ sudo apt install ansible
```

you must provide your username and password for run the playbook, use -K to
give a sudo password and -e 'USUARIO=<YOUR_USERNAME>' for playbook to know who
you are.

```
repository_dir$ ansible-playbook localsystem.yml -K -e 'USUARIO=zero'
```

take the liberty to read the roles you are interested to use, so you can just
run the features you want.

Have a nice coding!
